#GO 2 Tax Tab User Interface

tabItem(tabName = "go2tax",
        fluidRow(
          column(2,radioButtons(inputId="gotabDataGroupedVsIndiv_go2tax",
                          "Toggle Display of Individual vs Grouped Data", 
                          choices = c('Individual','Grouped'))), 
          column(2,radioButtons(inputId="bpccmf_go2tax",
                                "Toggle Display of GO Categories", 
                                choiceNames = c('Biological Process (BP)','Cellular Component (CC)','Molecular Function (MF)'),
                                choiceValues = c('Biological Process','Cellular Component','Molecular Function'))),
          column(2,radioButtons(inputId="inclusivity_go2tax",
                                "Toggle Inclusivity Selection of Tax IDs", 
                                choices = c('In Any','Only In All'))),
          column(5,offset=0,conditionalPanel(
            condition = "input.gotabDataGroupedVsIndiv_go2tax == 'Individual'",
            selectizeInput('goselectColumn_go2tax', 'Select fields to display', 
                        choices =  names(loadGOData_withbpccmftoggle_colnames()), 
                        selected =  names(loadGOData_withbpccmftoggle_colnames()),
                        multiple = TRUE, width="1000px")),
          conditionalPanel(
            condition = "input.gotabDataGroupedVsIndiv_go2tax == 'Grouped'",
            selectizeInput('goselectColumn2_go2tax', 'Select fields to display', 
                        choices = names(loadGOgroupedData_withbpccmftoggle_colnames()),
                        selected = names(loadGOgroupedData_withbpccmftoggle_colnames()),
                        multiple = TRUE, width="1000px"))),
          box(
            actionButton('resetSelection',label = "Reset"),
            #actionButton('selectAll',label = "Select All"),
            #downloadLink('downloadGOTabData_go2tax', label = "Download", style='padding:4px; font-size:100%;'),
            
            title = "Gene Ontology Data",
              DT::dataTableOutput("GOTable_go2tax"),
              style = "height:900px; overflow-y: scroll;overflow-x: scroll;",
              width = 12
          ),
          box(title = "Count Data Filtered by Selected Gene Ontology Data",
              DT::dataTableOutput("GOCountTable_go2tax"),
              style = "height:900px; overflow-y: scroll;overflow-x: scroll;",
              width = 12
              
          )
        ))