#Host Abundances Tab User Interface

tabItem(tabName = "hostabundances",
        fluidRow(
          column(5,offset=0,
            selectizeInput('selectColumn_hostabundances', 'Select fields to display', 
                           choices =  names(load_host_abundances_colnames()), 
                           selected =  names(load_host_abundances_colnames()),
                           multiple = TRUE, width="1000px")),
          box(
            actionButton('resetSelection_hostabundancesTable',label = "Reset"),
            #div(style="display:inline-block",downloadLink('downloadCountTabData', label = "Download", style='padding:4px; font-size:100%'), style="float:right"),
            #  ,
            title = "Host Abundances Data",
            DT::dataTableOutput("hostabundancesTable"),
            style = "height:900px; overflow-y: scroll;overflow-x: scroll;",
            width = 12
          )
        ))