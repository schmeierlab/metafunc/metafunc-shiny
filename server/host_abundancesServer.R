#Back end code for the host_abundances tab

## Code for rendering data table
output$hostabundancesTable <- DT::renderDataTable(server=TRUE,options = datatableoptions(),filter = 'top', rownames=F,escape=F,{
                                           datc<-load_host_abundances()
                                           datc<-datc[,input$selectColumn_hostabundances]
                                           #datc$gene<-sprintf('<a href=https://asia.ensembl.org/Multi/Search/Results?q=%s;site=ensembl_all target="_blank" ">%s</a>',datc$gene,datc$gene)
                                           datc})

output$downloadhostabundancesData <- downloadHandler(
  filename = function() {
    paste('data-', Sys.Date(), '.csv', sep='')
  },
  content = function(con) {
      write.csv(load_host_abundances(), con)
    } 
  
  
)

tableProxy_hostabundancesTable <-  dataTableProxy('hostabundancesTable')
#myProxy = DT::dataTableProxy('dt')
observeEvent(input$resetSelection_hostabundancesTable, {
  DT::selectRows(tableProxy_hostabundancesTable,NULL)
})