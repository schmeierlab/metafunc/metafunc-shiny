output$aboutpageinstructions <- renderUI({
  tags$div(
    "Welcome to the MetaFunc App, designed for interactive visualization and exploration of host and microbial abundance data, as well as gene ontology (GO) data and microbial taxonomy data generated from the MetaFunc Pipeline.",  tags$br(),  tags$br(), 
    "Full usage instructions with examples are located at: ", 
    tags$a(href="https://metafunc.readthedocs.io/en/latest/rshiny.html", "https://metafunc.readthedocs.io/en/latest/rshiny.html", target="_blank"), tags$br(), tags$br(),
    "Latest code is available at: ", 
    tags$a(href="https://gitlab.com/schmeierlab/metafunc/metafunc-shiny", "https://gitlab.com/schmeierlab/metafunc/metafunc-shiny", target="_blank"), 
    tags$br(), tags$br(),
    
    tags$strong("To get started:"), tags$br(),tags$br(),
    
    "1. ", tags$strong("Home Tab:"), tags$br(),
    tags$div(style = "padding-left: 20px;",
             "- Use the Home Page to familiarize yourself with the analysis steps of the MetaFunc Pipeline.", tags$br(),
    ), tags$br(), 
    
    "2. ", tags$strong("Microbiome Tab:"), tags$br(),
    tags$div(style = "padding-left: 20px;",
             "- Explore microbial abundance data in CPM (Counts-Per-Million) under the 'Abundances' sub-tab.", tags$br(),
             "- Use the 'Gene Ontology' sub-tab to view GO terms linked to specific samples, with options to filter by Biological Process, Cellular Component, or Molecular Function.", tags$br(),
             "- In the 'GO to TaxIDs' sub-tab, investigate relationships between GO terms and microbial taxa by selecting GO terms in the top table to view the associated taxonomy data in the table below.", tags$br(),
             "- In the 'TaxIDs to GO' sub-tab, similar to 'Go to TaxIDs', but reversed - investigate relationships between microbial taxa and GO terms by selecting microbes in the top table to view the associated GO data in the table below.", tags$br(),
    ), tags$br(),
    
    "3. ", tags$strong("Host Tab:"), tags$br(),
    tags$div(style = "padding-left: 20px;",
             "- Examine host gene expression abundances TPM (Transcripts-Per-Million) data under the 'Abundances' sub-tab.", tags$br(),
             "- Similar to the Microbiome tab, this tab allows filtering, sorting, and searching of human RNA-seq count data."
    ), tags$br(), 
    
    "4. ", tags$strong("Exporting Data:"), tags$br(),
    tags$div(style = "padding-left: 20px;",
             "- After filtering or selecting data of interest, use the download options provided to export the tables in .csv or .excel formats for further analysis."
    ), tags$br(), 
    
    "5. ", tags$strong("Individual vs. Grouped Data:"), tags$br(),
    tags$div(style = "padding-left: 20px;",
             "- The MetaFunc App can visualize both individual and grouped data in the Abundances, Gene Ontology, GO to TaxIDs, and TaxIDs to GO tabs. If grouped analysis was specified in the MetaFunc pipeline, switch the toggle to grouped to view your data."
    ), tags$br(),
    
    "6. ", tags$strong("GO and TaxID Data Inclusivity:"), tags$br(),
    tags$div(style = "padding-left: 20px;",
             "- In the GO to TaxIDs and TaxIDs to GO tabs, the user can toggle 'In Any' vs 'Only in All' to view associated terms (in the bottom table) that are either in any of the highlighted rows in the top table, or only present in all of the rows."
    ), tags$br(),
    
    "7. ", tags$strong("App Data Input:"), tags$br(),
    tags$div(style = "padding-left: 20px;",
             "- To change the app input data for subsequent runs of the MetaFunc pipeline, update path of the database target file variable 'dbPath' in initVars.R to match your latest database."
    ), tags$br(),
  )
})

output$aboutpageauthorinfo <- renderUI({
  tags$div(
    "App created by Tyler Kolisnik. MetaFunc Pipeline created by Arielle Sulit. Lab of Dr. Sebastian Schmeier, Massey University, Auckland, New Zealand"
  )
})